# Scones

## Ingredients

- 1 1/2 cups All-purpose Flour
- 1 1/2 cups White Whole Wheat Flour
- 1/2 cup Sugar
- 1 tablespoon Baking Powder
- 1/2 teaspoon Baking Soda
- 1/4 teaspoon Salt
- 8 tablespoons Butter
- 1 to 1 1/4 cups Buttermilk

## Instructions

1. Preheat oven to 450 F
2. Mix dry ingredients in a bowl.
3. Cut-in butter into dry ingredients. Butter should be roughly the size of a pea or larger.
4. Add 1 cup of Buttermilk then ¼ cup if needed.
5. Mix with a spoon or hands until dough comes together. Do not use a mixer.
6. If adding fruit, nuts, or chips, add half and fold the dough a couple times then add the remainder and fold a two or three times.
7. Place the dough on a floured surface and pat or roll into a ¾ to 1 inch tall eight to nine inch round.
8. Using a floured knife or a bakers knife (scraper), cut straight down and up and apply flour to the knife for each cut until you have eight wedges.
9. Place the scones onto a baking tray ½ to 1 inch apart on baking parchment paper.
10. Bake at 450 F until edge of scones begin brown or use a toothpick to check. The bake time will depend on the dryness of the dough but will be about 20 minutes. Recommend checking at 15 minutes or less. Scone tops will be brown with edges just starting to brown.

## Tips

- 3 cups All-purpose flour can be used if you don't have White Whole Wheat flour.
- If using flour alternatives other than White Whole Wheat, use 2 to 2 1/2 cups All-purpose flour and the difference of the alternative flour.
- Milk can be used if you do not have buttermilk.
- Brown sugar can be used instead of sugar. It is a nice touch for variations with Walnuts or cinnamon.
- Temperature is important. Butter must be cold when cutting into the flour. Minimize contact with your hands as you work with the dough.
- Chilling the mixing bowl and butter in the refrigerator and the dry ingredients in the freezer for 15 to 20 minutes helps if you are working in a hot kitchen during a very warm summer day.
- Fold dough sparingly. The resulting dough should be rough or scraggly not smooth.
- The dough is low in moisture relative to many breads. Slight sticky is fine and will provide moister scones. However, if the scones flow like cookies during baking then add less buttermilk.
- If using mashed bananas, it is deceptive on how much liquid to add. Reserve half or more of the liquid and mix by hand until you can gauge how much more liquid to add. Also, the bananas will make for a very sticky dough.

## Variations

### Almond Scones
- Add 3 to 4 tsp Almond extract to base recipe
- Add 1 cup chopped Almonds 

### Pecan Scones
- Add 2 tsp Orange extract
- Add 1 cup chopped Pecans

### Blueberry Scones
- Add 3 to 4 tsp Vanilla extract
- Add 1 cup Dried Blueberries

### Blueberry & White Chocolate Scones
- Add 1 to 2 tsp Vanilla extract
- Add ½ cup Dried Blueberries
- Add ½ cup White Chocolate chips

### Cinnamon & Raisins (& Walnut) Scones
- Add 1 to 2 tsp Cinnamon
- Add 1 cup Raisins
- (Add 1 cup chopped Walnuts)

### Cherry & White Chocolate Scones
- Add 1 to 2 tsp Vanilla extract
- ½ cup Dried Cherries
- ½ cup White Chocolate chips

### Chocolate Scones
- Add 2 to 3 tsp Vanilla extract
- Add 1 cup Chocolate chips; semi-sweet and minis if you got them

## Walnut & Chocolate Scones
- Add 1 to 2 tsp Vanilla extract
- Add ½ cup chopped Walnuts
- Add ½ cup Chocolate chips; semi-sweet and minis if you got them

### White Chocolate Scones
- Add 3 to 4 tsp Vanilla extract
- Add 1 cup White Chocolate chips

_Inspired from "King Arthur Flour 200th Anniversary Cookbook"_
