# Whole-grain Banana Bread

## Ingredients

- 1 cup (120g) All-purpose Flour
- 1 cup (113g) White Whole Wheat Flour
- 1 cup (213g) Brown Sugar, packed
- 1 tsb Cinnamon
- 1 tsp Baking Soda
- 1/2 tsp Baking Powder
- 3/4 tsp Salt
- 2 cups (454g) Mashed Bananas
- 1 cup (115g) of Walnuts, chopped
- 2 large Eggs
- 2 tsb Vanilla Extract
- 1/2 cup (99g) Vegetable Oil

## Instructions

1. Preheat oven to 350 F
2. Prepare 9" x 5" loaf pan, lightly grease or oil
3. In a large bowl, mix mashed banana, oil, sugar, eggs, and vanilla
4. Combine dry ingredient until thoroughly combined
5. Pour into prepared loaf pan
6. Bake for 45 minutes then tent to prevent the top from burning
7. Bake for an additional 20 to 25 minutes for a total of 75 to 80 minutes or until loaf is 205 F
8. Let loaf cool for 15 to 20 minutes then loosen the edges and turn it out onto a rack to cool

## Variations

Replace Walnuts with dried fruit, chocolate chips, sunflower or pumpkin seeds, or other mix-ins of your choice.

## Tips

If using glass or stoneware, increase baking by 10 to 15 minutes.
