# Pancakes & Waffles

## Ingredients

- 4 cups flour
    - 2 cups All-purpose flour
    - 2 cups White Whole Wheat flour
- 4 tablespoons sugar
- 1 tablespoon baking powder
- 1 teaspoon baking soda
- 1 teaspoon salt
- 4 eggs
- 3 to 4 cups milk, buttermilk, or yogurt
- 1/2 cup vegetable oil or 1 stick of melted butter (optional)

## Instructions

1. Mix dry ingrediants in a large bowl
2. Whisk milk and eggs until frothy then add oil
3. Add wet to dry ingredients
4. Mix until combine - do not overmix
5. For pancakes, use 1/3 to 1/2 cup batter and flip when bubbles pop and do not fill
6. For waffles, follow waffle iron instructions. Mine is 1 cup of batter.
7. Waffles are done when steam is mostly reduced. Mine is approx 2 minutes. 

## Variations

- 1 tablespoon ground cinnamon
- 1 to 2 cups mash bananas folded into batter
- 1 to 2 cups berries into batter
- 1 to 2 cups chopped apple with cinnamon
- 1 to 2 cups chopped apple with chedder cheese
- 1 teaspoon of cinnamon, nutmeg, and ginger and 1 cup mashed pumpkin and reduce liquid by approxiately 1 cup

## Tips

- Recipe serves three to four people, but can be halved for one or two people
- Excess pancakes and waffles can be frozen
- If waffles splits or the waffle iron doesn't open easily, the waffle needed to cook for an additional time
- Oil is optional, but for waffles it will help prevent sticking to a non-stick waffle iron
- The potency of cinnamon varies so adjust as necessary

