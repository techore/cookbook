# Sweetened Whipped Cream

## Ingredients

- 1 cup whipping (heavy) cream
- 3 tablespoons of sugar
- 1 teaspoon vanilla (optional)

## Instructions

1. Pour whipping cream in mixer
2. Add sugar
3. Add vanilla
4. Set mixer to high but reduce to medium prior to peaks forming
5. Once soft peaks have formed it is ready to serve

## Tips

- Each cup of heavy cream will result with approxiately 2 cups of Sweetened Whipped cream.
- Vanilla can be replace with
    - 1 tsp grated lemon or orange peel
    - 1/2 tsp ground cinnamon
    - 1/2 tsp ground ginger
    - 1/2 tsp nutmug
    - 1/2 tsp almond extract
    - 1/2 tsp peppermint extract
    - 1/2 tsp rum extract
    - 1/4 tsp maple extract
- Over beaten cream will curdle. Do not discard but continue beat to make butter.
- Store excess whipped cream in an airtight container and place in the freezer. Remove and place into refrigerator to thaw then serve.
