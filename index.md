# Cookbook

## Breads

1. [Amadama Bread](breads/amadama_bread.md)
2. [Whole-grain Banana Bread](breads/whole-grain-banana_bread.md) 
3. [Pancakes & Waffles](breads/pancake-waffle.md)
4. [Scones](breads/scones.md)
5. [Country Brown Sourdough Bread](breads/country_brown-sourdough_bread.md)

## Cakes & Pies

1. [Classic Pie Crust](cakes-pies/classic-piecrust.md) 
2. [Smooth & Spicey Pumpkin Pie](cakes-pies/smooth_spicey-pumpkin_pie.md)
3. [Summer Drop Cake](cakes-pies/summer_drop_cake.md)

## Desserts

1. [Sweetened Whipped Cream](desserts/sweetened_whipped_cream.md)
