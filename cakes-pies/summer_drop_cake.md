# Summer Spoon Cake

## Ingredients

### Fruit

- 2 1/2 to 3 1/2 cups (450+ grams) of fruit
    - blackberries
    - raspberries
- 2 tablespoons (25g) granulated sugar
- Zest of 1 lemon
- 3 tablespoons (42g) lemon juice

### Batter

- 1/2 cup (60g) All-purpose flour
- 1/2 cup (48g) Almond flour
- 1/2 cup (99g) granulated sugar
- 1 teaspoon baking powder
- 1/2 teaspoon salt
- 1/4 cup (57g) milk
- 1 large egg
- 1 to 2 teaspoons of Vanilla extract
- 8 tablespoons (113g) butter

## Instructions

1. Prepare fruit by adding fruit, sugar, zest, and lemon juice to a medium bowl, gently toss, and, optionally, leave on counter for 30 minutes to 4 hours until glossy and juices begin seeping.
2. Preheat oven to 400F with rack in the center.
3. Mix flours, sugar, baking powder, and salt in a large bowl
4. Place butter into 8 x 8 inch baking dish and into the oven until soft and mostly melted, 4 to 6 minutes
5. Remove baking dish and swirl melted butter to coat sides then drain and combine with flour mixture
6. Using a small bowl, whisk milk, egg, and vanilla
7. Combine wet to dry ingredients and mix until combined
8. Transfer the resulting batter to the 8 x 8 inch baking pan
9. Gently place fruit mixture and resulting juice over the batter to mostly cover the top
10. Bake cake until golden brown and puffed for 30 to 35 minutes
11. Remove from oven and serve warm. Pairs nicely with whipped cream or ice cream.

## Tips

* Substitute the fruit of your choice including stawberries, blueberries, peaches, etc.
* Use fresh fruit
* Any baking dish may be used but the fruit should still be visible after baking
